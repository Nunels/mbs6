﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MBSmart6.Services;
using MBSmart6.Views;
using MBSmart6.Data;
using System.IO;

namespace MBSmart6
{
    public partial class App : Application
    {
        static DeviceDatabase deviceDatabase;
        static TestDatabase testDatabase;

        public static DeviceDatabase DeviceDatabase
        {
            get
            {
                if (deviceDatabase == null)
                {
                    deviceDatabase=new DeviceDatabase(Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        "Devices.db3"));
                }
                return deviceDatabase;
            }
        }

        public static TestDatabase TestDatabase
        {
            get
            {
                if (testDatabase == null)
                {
                    testDatabase = new TestDatabase(Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        "Tests.db3"));
                }
                return testDatabase;
            }
        }

        public App()
        {
            InitializeComponent();
#if DEBUG
            HotReloader.Current.Run(this);
#endif
            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
