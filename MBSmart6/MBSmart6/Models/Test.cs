﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MBSmart6.Models
{
    public class Test
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string Path { get; set; }
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string Status { get; set; }
    }
}
