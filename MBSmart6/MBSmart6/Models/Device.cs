﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MBSmart6.Models
{
    public class Device
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string IpAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Ssid { get; set; }
        public string WifiPwd { get; set; }
    }
}
