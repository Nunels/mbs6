﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MBSmart6.Models;
using SQLite;

namespace MBSmart6.Data
{
    public class DeviceDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public DeviceDatabase(string dbPath)
        {
            //create a new connection to the database specified by dbPath
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Device>().Wait();
        }
        public Task<List<Device>> GetDevicesAsync()
        {
            return _database.Table<Device>().ToListAsync();
        }

        public Task<Device> GetDeviceAsync(int id)
        {
            return _database.Table<Device>()
                .Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveDeviceAsync(Device device)
        {
            if (device.ID != 0)
            {
                return _database.UpdateAsync(device);
            }
            else
            {
                return _database.InsertAsync(device);
            }
        }
        public Task<int> DeleteDeviceAsync(Device device)
        {
            return _database.DeleteAsync(device);
        }
    }
}
