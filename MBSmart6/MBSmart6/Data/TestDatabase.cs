﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MBSmart6.Models;
using SQLite;

namespace MBSmart6.Data
{
    public class TestDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public TestDatabase(string dbPath)
        {
            //create a new connection to the database specified by dbPath
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Test>().Wait();
        }

        public Task<List<Test>> GetTestsAsync()
        {
            return _database.Table<Test>().ToListAsync();
        }

        public Task<Test> GetTestAsync(int id)
        {
            return _database.Table<Test>()
                .Where(i => i.ID == id).FirstOrDefaultAsync();
        }
        public Task<int> SaveTestAsync(Test test)
        {
            if (test.ID != 0)
            {
                return _database.UpdateAsync(test);
            }
            else
            {
                return _database.InsertAsync(test);
            }
        }
        public Task<int> DeleteTestAsync(Test test)
        {
            return _database.DeleteAsync(test);
        }

        public Task<List<Test>> GetTestsByDeviceAsync(int deviceId)
        {
            return _database.Table<Test>().Where(i => i.DeviceId == deviceId).
                ToListAsync();
        }
    }
}
