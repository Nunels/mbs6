﻿using Android;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MBSmart6.Views2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevicePage : ContentPage
    {
        public DevicePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            int deviceId = (BindingContext as Models.Device).ID;
            testsListView.ItemsSource = await App.TestDatabase.GetTestsByDeviceAsync(deviceId);
            statusLabel.Text = "Non Connesso";
        }

        private async void OnTestSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new TestPage
                {
                    BindingContext = e.SelectedItem as Models.Test
                });
            }
        }

        private async void OnNewTestClicked(object sender, EventArgs e)
        {
            Models.Test t = new Models.Test();
            t.DeviceId = (BindingContext as Models.Device).ID;
            t.DeviceName = (BindingContext as Models.Device).Name;
            await Navigation.PushAsync(new AddTestPage
            {
                BindingContext = t
            });
        }

        private async void OnEditClicked(object sender, EventArgs e)
        {
            Models.Device device = BindingContext as Models.Device;
            await Navigation.PushAsync(new AddDevicePage
            {
                BindingContext = device
            });
        }

        private async void OnDeleteClicked(object sender, EventArgs e)
        {
            Models.Device d = BindingContext as Models.Device;
            bool action = await DisplayAlert("Elimina Dispositivo", "Vuoi eliminare il dispositivo?", "Elimina", "Annulla");
            if (action)
            {
                await App.DeviceDatabase.DeleteDeviceAsync(d);
                await Navigation.PopAsync();
            }
        }

        private void OnConnectClicked(object sender, EventArgs e)
        {
            Intent intent = new Android.Content.Intent(Android.Provider.Settings.ActionWifiSettings);
            intent.SetFlags(ActivityFlags.NewTask);
            Android.App.Application.Context.StartActivity(intent);
        }
    }
}