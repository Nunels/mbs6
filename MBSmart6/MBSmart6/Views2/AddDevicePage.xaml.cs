﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MBSmart6.Models;

namespace MBSmart6.Views2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddDevicePage : ContentPage
    {
        public AddDevicePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            Models.Device d = BindingContext as Models.Device;
            if (d.Name==null)
            {
                Title = "Aggiungi Dispositivo";
            }
        }

        private async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            var device = (Models.Device)BindingContext;
            await App.DeviceDatabase.SaveDeviceAsync(device);
            await Navigation.PopAsync();

        }
    }
}