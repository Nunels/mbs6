﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MBSmart6.Views2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddTestPage : ContentPage
    {
        public AddTestPage()
        {
            InitializeComponent();
        }

        private async void OnStartClicked(object sender, EventArgs e)
        {
            var test = BindingContext as Models.Test;
            await App.TestDatabase.SaveTestAsync(test);
            await Navigation.PopAsync();
        }
    }
}