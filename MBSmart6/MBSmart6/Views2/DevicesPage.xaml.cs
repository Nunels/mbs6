﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MBSmart6.Models;

namespace MBSmart6.Views2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevicesPage : ContentPage
    {
        public DevicesPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            devicesListView.ItemsSource = (await App.DeviceDatabase.GetDevicesAsync());

        }

        private async void OnDeviceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem != null)
            {
                await Navigation.PushAsync(new DevicePage
                {
                    BindingContext = e.SelectedItem as Models.Device
                });
            }
        }

        private async void OnAddButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddDevicePage
            {
                BindingContext = new Models.Device()
            });
        }
    }
}