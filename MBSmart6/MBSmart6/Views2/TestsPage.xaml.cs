﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MBSmart6.Views2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestsPage : ContentPage
    {
        public TestsPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            testsListView.ItemsSource = await App.TestDatabase.GetTestsAsync();
        }

        private async void OnTestSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(e.SelectedItem != null)
            {
                await Navigation.PushAsync(new TestPage
                {
                    BindingContext = e.SelectedItem as Models.Test
                });
            }
        }

        private void OnNewTestClicked(object sender, EventArgs e)
        {

        }
    }
}